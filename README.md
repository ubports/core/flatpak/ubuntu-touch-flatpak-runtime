# Ubuntu Touch Flatpak Runtime

This is an attempt to package the Ubuntu Touch libraries as flatpak runtime,
with the goal of running Ubuntu Touch apps.
While some apps already work fine, the overall state is experimental.

![Ubuntu Touch apps running on the desktop in flatpak](screenshot.png)

## Install

The repository is built on GitLab ci. You can add it to your flatpak installation using:
```
flatpak remote-add --no-gpg-verify ubports https://gitlab.com/ubports/core/flatpak/ubuntu-touch-flatpak-runtime/-/jobs/artifacts/master/raw/repo/?job=apps
```

You can find out which applications exist using `flatpak remote-ls ubports`, and install them as usual with flatpak.

## Contributing

Please always do a full build of Sdk and apps before creating a merge request.
The CI resources are currently sparse, and needlessly failing pipelines would unnecessarily block the CI runner.

## Build Locally

Ensure that you have flatpak and flatpak-builder installed on your system first.

Clone the repository:

```
$ git clone https://gitlab.com/ubports/core/flatpak/ubuntu-touch-flatpak-runtime.git
$ cd ubuntu-touch-flatpak-runtime
```

Build the base packages as a repo, and then install them:

```
$ flatpak-builder --install-deps-from=flathub tmp --force-clean --ccache --repo repo com.ubports.Sdk.yml
$ flatpak remote-add --no-gpg-verify ubports file://$PWD/repo
$ flatpak install com.ubports.Platform com.ubports.Sdk
```

Build the applications you want (example with clock):

```
cd apps
flatpak-builder build --force-clean --ccache clock.json --install
flatpak run org.ubports.clock
```

If flatpak-builder commands fail with `error: Flatpak system operation ConfigureRemote not allowed for user`, try running them with sudo.
